﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour
{
    [SerializeField]
    Behaviour[] components;

    Camera sceneCamera;

    public GameObject bulletPrefab;
    public Transform bulletSpawn;

    void Start()
    {
        DisableComponents();
    }

    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);

        if (Input.GetButtonDown("Fire1"))
        {
            CmdFire();
        }
    }

    void DisableComponents()
    {
        if(!isLocalPlayer)
        {
            for(int cont = 0; cont < components.Length; cont++)
            {
                components[cont].enabled = false;
            }
        }
        else
        {
            sceneCamera = Camera.main;

            sceneCamera.gameObject.SetActive(false);
        }
    }

    // QUESTO CODICE [Command] VIENE CHIAMATO SUL CLIENT …
    // … MA ESEGUITO SUL SERVER !
    [Command]
    void CmdFire()
    {
        // SPAWN DEL PROIETTILE
        var bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);

        // AGGIUNGE LA VELOCITA' AL PROIETTILE PER FARLO CAMMINARE
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;

        // SPAWNA IL PROIETTILE SUL CLIENT
        NetworkServer.Spawn(bullet);

        // DISTRUGGER IL PROIETTILE DOPO 2 SECONDI
        Destroy(bullet, 2.0f);
    }

    public override void OnStartLocalPlayer()
    {
        GetComponent<MeshRenderer>().material.color = Color.blue;
    }

    void OnDisable()                        // QUANDO IL PLAYER SULL'HOST LOCALE VIENE DISATTIVATO
    {
        sceneCamera.gameObject.SetActive(true);             // ATTIVA LA CAMERA DI SCENA        
    }
}